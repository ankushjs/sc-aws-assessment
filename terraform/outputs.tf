output "lambda-arn" {
    value = "${aws_lambda_function.lambda_function.arn}"
}
output "state-machine-arn" {
    value = "${aws_sfn_state_machine.sfn_state_machine.arn}"
}
output "s3-bucket" {
    value = "${aws_s3_bucket.bucket.id}"
}