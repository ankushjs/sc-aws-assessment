resource "aws_dynamodb_table" "files_table" {
  name            = "${var.ddb_table_name}"
  billing_mode    = "PROVISIONED"
  read_capacity   = "1"
  write_capacity  = "1"
  hash_key        = "FileName"

  attribute {
    name = "FileName"
    type = "S"
  }
   tags = {
    Name        = "Files"
    Environment = "${var.environment}"
  }
}