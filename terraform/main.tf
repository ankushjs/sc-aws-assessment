terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 4.0"
   }
   archive = {
     source  = "hashicorp/archive"
     version = "~> 2.0"
   }
 }
}

# Fetching current account id

data "aws_caller_identity" "current" {}

# Packaging Lambda Function
data "archive_file" "lambda_function" {
  type             = "zip"
  source_file      = "lambda_function.py"
  output_file_mode = "0666"
  output_path      = "lambda_function.zip"
}