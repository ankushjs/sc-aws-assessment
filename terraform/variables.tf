# Creating resource variables
variable "environment" {
  type = string
}
variable "region" {
    type    = string
    description = "AWS Region to be used"
}
variable "s3_bucket_name" {
    type = string
    description = "The Name of The Dynamo DB Table where file names are to be put"
}
variable "ddb_table_name" {
    type = string
    description = "The Name of The Dynamo DB Table where file names are to be put"
}
variable "step_function_name" {
    type    = string
    description = "The Name of the Step Function that puts the file name in the Dynamo DB Table"
}
variable "localstack_endpoint" {
    type    = string
}
variable "lambda_function_name" {
    type = string
    description = "The Name of The Lambda Function that invokes the step function"
}
variable "lambda_handler_name" {
    type = string
    description = "The Name of The Lambda Handler"
}
variable "lambda_runtime" {
    type = string
    description = "Lambda Runtime"
}
variable "lambda_timeout" {
    type = number
    description = "Lambda Timeout"
}
