resource "aws_s3_bucket" "bucket" {
bucket = "${var.s3_bucket_name}"
tags = {
    Name        = "files-bucket"
    Environment = "${var.environment}"
    }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}