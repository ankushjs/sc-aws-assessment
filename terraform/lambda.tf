##IAM Role for Lambda

resource "aws_iam_role" "iam_role_for_lambda" {
  name               = "iam_role_for_lambda"
  assume_role_policy = file("lambda_assume_role_policy.json")
}

##IAM Role Policy for Lambda

resource "aws_iam_role_policy" "iam_policy_for_lambda" {
  name               = "iam_policy_for_lambda"
  role               = "${aws_iam_role.iam_role_for_lambda.id}"
  policy             = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "states:StartExecution",
            "Resource": "${aws_sfn_state_machine.sfn_state_machine.arn}",
            "Sid": "Allow Step Function Execution"
        }
    ]
})
}

##Lambda Resource

resource "aws_lambda_function" "lambda_function" {
function_name    = "${var.lambda_function_name}"
role             = "${aws_iam_role.iam_role_for_lambda.arn}"
handler          = "${var.lambda_handler_name}.lambda_handler"
runtime          = "${var.lambda_runtime}"
timeout          = "${var.lambda_timeout}"
filename         = "${var.lambda_handler_name}.zip"
source_code_hash = filebase64sha256("${var.lambda_handler_name}.zip")
environment {
   variables = {
    region = "${var.region}",
    state_machine_arn = "${aws_sfn_state_machine.sfn_state_machine.arn}"
    localstack_endpoint = "${var.localstack_endpoint}"
    }
}
}
## lambda trigger

resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
bucket = "${aws_s3_bucket.bucket.id}"

lambda_function {
    lambda_function_arn = "${aws_lambda_function.lambda_function.arn}"
    events              = ["s3:ObjectCreated:*"]
    }
}
resource "aws_lambda_permission" "test" {
    statement_id  = "AllowS3Invoke"
    action        = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.lambda_function.function_name}"
    principal     = "s3.amazonaws.com"
    source_arn    = "arn:aws:s3:::${aws_s3_bucket.bucket.id}"
}
