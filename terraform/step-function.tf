resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "${var.step_function_name}"
#  role_arn = aws_iam_role.iam_for_sfn.arn
  role_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AWSRoleForStepFunction"
  definition = jsonencode({
    "Comment": "Put File Name to DynamoDB Table using Step Function",
    "StartAt": "DynamoDB Put File Name",
    "States": {
      "DynamoDB Put File Name": {
        "Type": "Task",
        "Resource": "arn:aws:states:::dynamodb:putItem",
        "Parameters": {
          "TableName": "${var.ddb_table_name}",
          "Item": {
            "FileName": {
              "S.$": "$.FileName"
            }
          }
        },
        "End": true
      }
    }
})
}