import os
import json
import boto3

region = os.environ['region']
state_machine_arn =  os.environ['state_machine_arn']
endpoint_url= os.environ['localstack_endpoint']

print(region)
print(state_machine_arn)

def lambda_handler(event, context):
#lambda function to get the file name from s3 put event trigger and invoke step function with the file name as input parameter
    print(region)
    print(state_machine_arn)
    sts = boto3.client('sts', region_name=region, endpoint_url=endpoint_url)
    print(sts.get_caller_identity())
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    print(file_key)
    
    stepFunction = boto3.client('stepfunctions', region_name=region, endpoint_url=endpoint_url)
    response = stepFunction.start_execution(
        stateMachineArn=state_machine_arn,
        input = json.dumps({'FileName': file_key})
    )
    
    return print(response)